class TempTracker {
    constructor() {
        this.temperatures = [];
    }

    all() {
        return [...this.temperatures];
    }

    clear() {
        this.temperatures.length = 0;
    }

    count() {
        return this.temperatures.length;
    }

    insert(temperature) {
        if (typeof temperature !== 'number' || Number.isNaN(temperature)) {
            throw new TypeError('must be a number');
        }

        this.temperatures.push(temperature);
    }

    add(temperature) {
        this.insert(temperature);
    }

    get length() {
        return this.count();
    }

    get lowest() {
        return this.check((temperatures) => Math.min(...temperatures));
    }

    get highest() {
        return this.check((temperatures) => Math.max(...temperatures));
    }

    get average() {
        return this.check((temperatures, length) => {
            const totalTemperature = temperatures.reduce(
                (acc, temperature) => acc + temperature,
                // eslint-disable-next-line
                0
            );

            return Number((totalTemperature / length).toFixed(2));
        });
    }

    // should be private method
    check(callback) {
        if (typeof callback !== 'function') {
            throw new TypeError('callback must be a function');
        }

        if (this.length === 0) {
            throw new Error('add at least one temperature value');
        }

        return callback(this.temperatures, this.length);
    }
}

module.exports = TempTracker;
