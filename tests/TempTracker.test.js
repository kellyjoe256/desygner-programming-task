const { expect } = require('chai');
const TempTracker = require('../src/TempTracker');

describe('TempTracker', () => {
    const temperatures = [31, -3, 12.5, 89.0];
    const temperatureTracker = new TempTracker();

    beforeEach(() => {
        temperatureTracker.clear();
        temperatures.forEach((temperature) => {
            temperatureTracker.add(temperature);
        });
    });

    describe('all()', () => {
        it('should return a copy of all the entered temperatures', () => {
            expect(temperatureTracker.all()).to.include.members(temperatures);
        });
    });

    describe('clear()', () => {
        it('should be able to clear all entered temperatures', () => {
            temperatureTracker.clear();

            expect(temperatureTracker.all()).to.have.lengthOf(0);
            expect(temperatureTracker.count()).to.be.equal(0);
            expect(temperatureTracker.length).to.be.equal(0);
        });
    });

    describe('count()', () => {
        it('should return the count of the current temperatures', () => {
            expect(temperatureTracker.count()).to.be.equal(
                // eslint-disable-next-line
                temperatures.length
            );
        });
    });

    describe('add()', () => {
        it('should be able to add to current temperatures if any', () => {
            const additionalTemperatures = [32.5, 12.8];
            additionalTemperatures.forEach((temperature) => {
                temperatureTracker.add(temperature);
            });
            const allTemperatures = [
                ...temperatures,
                ...additionalTemperatures,
            ];

            expect(temperatureTracker.all()).to.include.members(
                // eslint-disable-next-line
                allTemperatures
            );
            expect(temperatureTracker.count()).to.be.equal(
                // eslint-disable-next-line
                allTemperatures.length
            );
            expect(temperatureTracker.length).to.be.equal(
                // eslint-disable-next-line
                allTemperatures.length
            );
        });

        it('should throw an error if try to add a non-numeric value', () => {
            const values = ['1', undefined, null, true, {}, NaN];
            values.forEach((value) => {
                expect(() => temperatureTracker.add(value)).to.throw();
                expect(() => temperatureTracker.add(value)).to.throw(TypeError);
            });
        });
    });

    describe('insert()', () => {
        it('should be able to add to current temperatures if any', () => {
            const additionalTemperatures = [32.5, -12, 12.8];
            additionalTemperatures.forEach((temperature) => {
                temperatureTracker.insert(temperature);
            });
            const allTemperatures = [
                ...temperatures,
                ...additionalTemperatures,
            ];

            expect(temperatureTracker.all()).to.include.members(
                // eslint-disable-next-line
                allTemperatures
            );
            expect(temperatureTracker.count()).to.be.equal(
                // eslint-disable-next-line
                allTemperatures.length
            );
            expect(temperatureTracker.length).to.be.equal(
                // eslint-disable-next-line
                allTemperatures.length
            );
        });

        it('should throw an error if try to add a non-numeric value', () => {
            const values = ['1', undefined, null, true, {}, NaN];
            values.forEach((value) => {
                expect(() => temperatureTracker.insert(value)).to.throw();
                expect(() => temperatureTracker.insert(value)).to.throw(
                    // eslint-disable-next-line
                    TypeError
                );
            });
        });
    });

    describe('length', () => {
        it('should return the count of the current temperatures', () => {
            expect(temperatureTracker.length).to.be.equal(
                // eslint-disable-next-line
                temperatures.length
            );
        });
    });

    describe('highest', () => {
        it('should return the highest temperature entered so far', () => {
            expect(temperatureTracker.highest).to.be.equal(
                // eslint-disable-next-line
                Math.max(...temperatures)
            );
        });

        it('should throw an error if there are no temperatures', () => {
            temperatureTracker.clear();

            expect(() => temperatureTracker.highest).to.throw();
        });
    });

    describe('lowest', () => {
        it('should return the lowest temperature entered so far', () => {
            expect(temperatureTracker.lowest).to.be.equal(
                // eslint-disable-next-line
                Math.min(...temperatures)
            );
        });

        it('should throw an error if there are no temperatures', () => {
            temperatureTracker.clear();

            expect(() => temperatureTracker.lowest).to.throw();
        });
    });

    describe('average', () => {
        it('should return the average of all the temperatures', () => {
            // prettier-ignore
            const average = temperatures.reduce((acc, temp) => acc + temp, 0) / temperatures.length;

            expect(temperatureTracker.average).to.be.equal(
                // eslint-disable-next-line
                Number(average.toFixed(2))
            );
        });

        it('should throw an error if there are no temperatures', () => {
            temperatureTracker.clear();

            expect(() => temperatureTracker.average).to.throw();
        });
    });
});
